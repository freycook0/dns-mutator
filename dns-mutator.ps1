# https://www.punycoder.com/c-sharp/

function Dns-Mutator {
	Param(
		[parameter(Mandatory=$True, ParameterSetName="Domain", ValueFromPipeline=$true)]
		[String]
		$domain
	)

	# If the domain provided is not actually a domain name, then quit.
	if([System.Uri]::CheckHostName($domain) -ne [System.UriHostNameType]::Dns){
		Write-Error "$domain is not a domain."
		return
	}

	# Make everything lowercase.  The hash table with the IDN character homoglyphs is based on lower-case characters.  If internationalized domains are to be supported, will have to figure out ToLower() for international locales.
	$originalDomain = $domain
	$domain = $domain.ToLower()

	# Check if $domain is an IDN.  Only non-IDN domains are currently supported.
	$map = New-Object System.Globalization.IdnMapping
	if($domain -ne $map.GetAscii($domain)){
		Write-Error "Dns-Mutator only supports non-IDN domains."
		return
	}

	# Split the domain into 2 parts: the sub-domain name, and the rest of the domain (2nd section through the gtld).
	$n = $domain.IndexOf('.')
	$domainPieces = $domain.Split('.')
	$first = $domainPieces[0]
	$rest = $domainPieces[1..($domainPieces.Count-1)]
	$restString = $rest -Join '.'

	# Get the XML-formatted hash table with the IDN character homoglyphs.
	$path = (Get-Item ((pwd).Path)).FullName
	$substitutions = Import-Clixml "$path\homoglyphs.xml"

	# ArrayList to contain all the permutations, including the original sub-domain.
	$permutations = New-Object System.Collections.ArrayList
	$permutations.Add($first) | Out-Null

	# Go character by character.  For each character, permutate through every possible IDN homoglyph.  Each permutation gets added to the list.
	for($i = 0; $i -lt $first.Length; $i++){
		$letter = [string]($first[$i])
		$subs = $substitutions[$letter]
		foreach($letterSub in $substitutions[$letter]){
			if($i -eq ($first.Length-1)){
				$firstSubString = $first.Substring(0,($first.Length-1))
				$substitutedString = "$firstSubString$letterSub"
				$permutations.Add($substitutedString) | Out-Null
			}
			else{
				$firstSubString1 = $first.Substring(0,$i)
				$firstSubString2 = $first.Substring($i+1)
				$substitutedString = "$firstSubString1$letterSub$firstSubString2"
				$permutations.Add($substitutedString) | Out-Null
			}
		}
	}

	# Begin building dataset: IDN, Punycode Domain Name, Primary DNS server (if found), Name Administrator (if found), and A/AAAA address(es) (if found).
	$idnHomoglyphs = New-Object System.Collections.ArrayList
	$dnsResolver = '1.1.1.1'
	foreach($permutation in $permutations){
		$permutatedDomain = "$permutation.$restString"
		$punycode = $map.GetAscii($permutatedDomain)
		$dnsServer = ''
		$nameAdmin = ''
		$ipString = ''
		$soa = (Resolve-DnsName -Name $punycode -Server $dnsResolver -DnssecOk -DnsOnly -Type SOA) 2> $null
		$dnsServer = $soa.PrimaryServer
		$nameAdmin = $soa.NameAdministrator
		$ips = (Resolve-DnsName -Name $punycode -Server $dnsResolver -DnssecOk -DnsOnly).IPAddress 2> $null
		$ipstmp = ""
		foreach($ip in $ips){
			if($ipString -eq ""){
				$ipString = $ip
			}
			else{
				$ipString = "$ipString,$ip"
			}
		}
		$idnHomoglyph = [PSCustomObject]@{
			OriginalDomain = $domain
			DomainNameInt = $permutatedDomain
			DomainNamePunyCode = $punycode
			PrimaryDomainNameServer = $dnsServer
			NameAdministratorServer = $nameAdmin
			IPAddresses = $ipString
		}
		$idnHomoglyphs.Add($idnHomoglyph) | Out-Null
	}

	# Print out the data
	$a = @{Expression={$_.OriginalDomain}; Label="Original Domain"; Width=17},
		@{Expression={$_.DomainNameInt}; Label="Domain Name Int"; Width=20},
		@{Expression={$_.DomainNamePunyCode}; Label="Domain Name Puny Code"; Width=25},
		@{Expression={$_.PrimaryDomainNameServer}; Label="Primary Domain Name Server"; Width=30},
		@{Expression={$_.NameAdministratorServer}; Label="Name Administrator Server"; Width=30},
		@{Expression={$_.IPAddresses}; Label="IP Addresses"; Width=50}
	$idnHomoglyphs | Format-Table -Property $a -Wrap # DomainNameInt, DomainNamePunyCode, PrimaryDomainNameServer, NameAdministratorServer, IPAddresses -AutoSize -Wrap

}

foreach($Arg in $Args){
	Dns-Mutator -Domain $Arg
}
